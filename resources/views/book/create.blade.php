@extends('layouts.app')
@section('content').

    <h1>Editar datos de libro</h1>
    <div class="form">
    <form  action="/books" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Titulo: </label>
        <input type="text" name="title" value="{{ old('title') }}">
        {{ $errors->first('title') }}
    </div>
    <div class="form-group">
        <label>Paginas: </label>
        <input type="text" name="pages" value="{{ old('pages') }}">
        {{ $errors->first('pages') }}
    </div>
    
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')
@extends('layouts.app')

@section('content')
    <h1>Lista de libros</h1>
    
        <a href="/books/create">Nuevo</a>
    
        
    
    <table class="table">
        <thead>
            <tr>
                <th>Nombre de Usuario de alta</th>
                <th>Genero</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($books as $book)
            <tr>
                <td>  {{ $book->gender->name }} </td>
                <td>  
                    <form method="post" action="/books/{{ $book->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $book)
                            <input type="submit" value="Borrar">
                        @endcan

                        @can('update', $book)
                            <a href="/books/{{ $book->id }}/edit">Editar</a>
                        @endcan
                        <a href="/books/{{ $book->id }}"> Ver </a>
                    </form>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $books->render() }}

@endsection('content')
<?php

namespace App\Policies;

use App\User;
use App\Book;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy
{
    use HandlesAuthorization;

     public function view(User $user, Book $book)
    {
        //
    }

    
    public function create(User $user)
    {
        return true;
    }

    
    public function update(User $user, Book $book)
    {
        //
    }

    
    public function delete(User $user, Book $book)
    {
        return $user->id === $book->user_id;
    }
}   


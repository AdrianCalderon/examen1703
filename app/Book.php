<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }
}
